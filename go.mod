module github.com/emmansun/gmsm

go 1.15

require (
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9
)
